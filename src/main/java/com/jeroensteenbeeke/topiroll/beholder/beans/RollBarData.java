package com.jeroensteenbeeke.topiroll.beholder.beans;

public interface RollBarData {
	String getServerKey();

	String getClientKey();

	String getEnvironment();
}